## 1.0.2

- Make the type of the tuple returned by `load` and `loads` variable-length

  It was previously erroneously set to a length of 1

## 1.0.1

- Add missing `py.typed` file so that type checkers can collect type information
