import unittest

from benparse import loads, StrictError


class TestBencodeLoad(unittest.TestCase):
    def test_string(self) -> None:
        # basic string
        self.assertEqual(loads(b'3:FOO'), b'FOO')
        # longer string. length is two digits
        self.assertEqual(
            loads(b'26:THIS IS A LONG TEST STRING'),
            b'THIS IS A LONG TEST STRING'
        )
        # zero-length string
        self.assertEqual(loads(b'0:'), b'')
        # int
        self.assertEqual(loads(b'2:68'), b'68')
        # binary data
        self.assertEqual(
            loads(
                b'14:\x8c\xe1\x94\x79\x82\xcd\x94\x4c\x82\xc5\x82\xa0\x82\xe9'
            ),
            b'\x8c\xe1\x94\x79\x82\xcd\x94\x4c\x82\xc5\x82\xa0\x82\xe9'
        )

        # length is too long
        self.assertRaises(ValueError, loads, b'4:FOO')
        # length is too short
        self.assertRaises(ValueError, loads, b'2:FOO')
        # length is negative
        self.assertRaises(ValueError, loads, b'l3:FOO-5:3:BARe')
        # missing length
        self.assertRaises(ValueError, loads, b':FOO')
        # missing colon
        self.assertRaises(ValueError, loads, b'3FOO')
        # missing colon, with matching length
        self.assertRaises(ValueError, loads, b'2FOO')

    def test_int(self) -> None:
        # single digit
        self.assertEqual(loads(b'i8e'), 8)
        # large number
        self.assertEqual(loads(b'i305672e'), 305672)
        # negative
        self.assertEqual(loads(b'i-28e'), -28)
        # zero
        self.assertEqual(loads(b'i0e'), 0)

        # not an int
        self.assertRaises(ValueError, loads, b'iSTRINGe')
        # missing closing 'e'
        self.assertRaises(ValueError, loads, b'i23')
        # float
        self.assertRaises(ValueError, loads, b'i2.3e')

    def test_list(self) -> None:
        # list of ints
        self.assertEqual(loads(b'li23ei67ei2ei567ee'), (23, 67, 2, 567))
        # list of strings
        self.assertEqual(
            loads(b'l3:FOO6:STRING0:11:ASSERTEQUALe'),
            (b'FOO', b'STRING', b'', b'ASSERTEQUAL')
        )
        # list of dicts
        self.assertEqual(
            loads(b'ld3:BARi10e3:FOOi20eed2:K15:APPLE4:KEY26:ORANGEee'),
            (
                {b'BAR': 10, b'FOO': 20},
                {b'K1': b'APPLE', b'KEY2': b'ORANGE'}
            )
        )
        # mixed list
        self.assertEqual(
            loads(b'l4:BOATi87ei-62e9:DELIMITERe'),
            (b'BOAT', 87, -62, b'DELIMITER')
        )
        # nested lists
        self.assertEqual(
            loads(b'lleli1ei2ee1:A2:BCli12e4:FOURee'),
            (
                tuple(),
                (1, 2),
                b'A',
                b'BC',
                (12, b'FOUR')
            )
        )
        # deep nested lists
        self.assertEqual(
            loads(b'llll3:FOO3:BARee4:BYTE6:STRINGee'),
            (
                (
                    (
                        (b'FOO', b'BAR'),
                    ),
                    b'BYTE',
                    b'STRING'
                ),
            )
        )
        # empty list
        self.assertEqual(loads(b'le'), tuple())

        # missing closing 'e'
        self.assertRaises(ValueError, loads, b'l')
        # missing closing 'e' with data
        self.assertRaises(ValueError, loads, b'li12ei54e')
        # nested missing closing 'e'
        self.assertRaises(ValueError, loads, b'lle')

    def test_dict(self) -> None:
        # dict of strings
        self.assertEqual(
            loads(b'd3:BAR5:APPLE11:EMPTYSTRING0:3:FOO6:ORANGEe'),
            {
                b'BAR': b'APPLE',
                b'EMPTYSTRING': b'',
                b'FOO': b'ORANGE'
            }
        )
        # dict of ints
        self.assertEqual(
            loads(b'd7:INTEGERi-55e4:KEY2i12e4:KEY3i14ee'),
            {
                b'INTEGER': -55,
                b'KEY2': 12,
                b'KEY3': 14
            }
        )
        # dict of lists
        self.assertEqual(
            loads(b'd8:INT_DICTli23ei77ei31ee8:STR_DICTl3:FOO3:BARee'),
            {
                b'INT_DICT': (23, 77, 31),
                b'STR_DICT': (b'FOO', b'BAR')
            }
        )
        # nested dicts
        self.assertEqual(
            loads(
                b'd4:DIC1d2:K13:FOO2:K2i32ee5:DICT2d3:KEY5:VALUE4:KEY2i79eee'
            ),
            {
                b'DIC1': {
                    b'K1': b'FOO',
                    b'K2': 32
                },
                b'DICT2': {
                    b'KEY': b'VALUE',
                    b'KEY2': 79
                }
            }
        )
        # deep nested lists
        self.assertEqual(
            loads(
                b'd2:D1d2:D2d2:D3d2:L4li1ei2ei3ee2:S410:DEEPSTRINGee'
                b'2:L2l1:A1:B1:Ceee'
            ),
            {
                b'D1': {
                    b'D2': {
                        b'D3': {
                            b'L4': (1, 2, 3),
                            b'S4': b'DEEPSTRING'
                        }
                    },
                    b'L2': (b'A', b'B', b'C')
                }
            }
        )
        # empty dict
        self.assertEqual(loads(b'de'), {})

        # missing closing 'e'
        self.assertRaises(ValueError, loads, b'd')
        # missing closing 'e' with data
        self.assertRaises(ValueError, loads, b'd3:KEYi54e')
        # nested missing closing 'e'
        self.assertRaises(ValueError, loads, b'd3:KEYde')
        # key is an int
        self.assertRaises(TypeError, loads, b'di16e3:FOOe')
        # key is a list
        self.assertRaises(TypeError, loads, b'dle3:FOOe')
        # key is a dict
        self.assertRaises(TypeError, loads, b'dde3:FOOe')
        # duplicate key
        self.assertRaises(ValueError, loads, b'd3:KEYi1e3:KEYi2ee')
        # mismatched number of keys and values
        self.assertRaises(ValueError, loads, b'd4:KEY14:VAL14:KEY2e')

    def test_general(self) -> None:
        # unexpected 'e' delimiters
        self.assertRaises(ValueError, loads, b'e')
        self.assertRaises(ValueError, loads, b'3:FOOe')

        # multiple top-level values
        self.assertRaises(ValueError, loads, b'i0e3:BAR')
        self.assertRaises(ValueError, loads, b'i0ei1e')
        self.assertRaises(ValueError, loads, b'i0ele')
        self.assertRaises(ValueError, loads, b'i0ede')

        # invalid types return type error
        self.assertRaises(TypeError, loads, 'UNICODE')

        # bytearray
        self.assertEqual(loads(bytearray(b'l3:FOOi43ee')), (b'FOO', 43))

        # empty string returns None
        self.assertEqual(loads(b''), None)

    def test_strict(self) -> None:
        # negative zero
        self.assertRaises(StrictError, loads, b'i-0e', strict=True)
        self.assertEqual(loads(b'i-0e', strict=False), 0)
        # leading zero
        self.assertRaises(StrictError, loads, b'i047e', strict=True)
        self.assertEqual(loads(b'i047e', strict=False), 47)
        # multiple leading zeros
        self.assertRaises(StrictError, loads, b'i007e', strict=True)
        self.assertEqual(loads(b'i007e', strict=False), 7)
        # only zero with leading zeros
        self.assertRaises(StrictError, loads, b'i00e', strict=True)
        self.assertEqual(loads(b'i00e', strict=False), 0)
        # negative and leading zero
        self.assertRaises(StrictError, loads, b'i-047e', strict=True)
        self.assertEqual(loads(b'i-047e', strict=False), -47)

        # length has leading zero
        self.assertRaises(StrictError, loads, b'03:FOO', strict=True)
        self.assertEqual(loads(b'03:FOO', strict=False), b'FOO')
        # length is negative zero
        self.assertRaises(StrictError, loads, b'-0:', strict=True)
        self.assertEqual(loads(b'-0:', strict=False), b'')

        # keys out of order
        self.assertRaises(
            StrictError, loads,
            b'd4:KEY14:VAL14:KEY34:VAL24:KEY24:VAL3e', strict=True
        )
        self.assertEqual(
            loads(b'd4:KEY14:VAL14:KEY34:VAL24:KEY24:VAL3e', strict=False),
            {
                b'KEY1': b'VAL1',
                b'KEY3': b'VAL2',
                b'KEY2': b'VAL3'
            }
        )
        # keys out of order in nested dict
        self.assertRaises(
            StrictError, loads,
            b'd7:2_DICT1d6:2_KEY14:VAL16:1_KEY24:VAL2ee', strict=True
        )
        self.assertEqual(
            loads(b'd7:2_DICT1d6:2_KEY14:VAL16:1_KEY24:VAL2ee', strict=False),
            {
                b'2_DICT1': {
                    b'2_KEY1': b'VAL1',
                    b'1_KEY2': b'VAL2'
                }
            }
        )
        # keys not out of order in nested dict. shouldn't raise an exception
        self.assertEqual(
            loads(b'd7:2_DICT1d6:1_KEY14:VAL16:1_KEY24:VAL2ee', strict=True),
            {
                b'2_DICT1': {
                    b'1_KEY1': b'VAL1',
                    b'1_KEY2': b'VAL2'
                }
            }
        )

        # strict should be True by default
        self.assertRaises(StrictError, loads, b'i-0e')

    def test_encoding(self) -> None:
        # decode basic string
        self.assertEqual(
            loads(bytes('9:文字列', 'utf_8'), encoding='utf_8'),
            '文字列'
        )
        # different encoding
        self.assertEqual(
            loads(bytes('6:文字列', 'shift_jis'), encoding='shift_jis'),
            '文字列'
        )
        # decode dict
        self.assertEqual(
            loads(bytes('d4:KEY13:値6:キーi23ee', 'utf_8'), encoding='utf_8'),
            {
                'KEY1': '値',
                'キー': 23
            }
        )
        # decode list
        self.assertEqual(
            loads(bytes('l7:UNICODEi45e3:値e', 'utf_8'), encoding='utf_8'),
            ('UNICODE', 45, '値')
        )
        # decode empty string
        self.assertEqual(loads(b'0:', encoding='utf_8'), '')
        # dict keys out of order should still raise an exception
        self.assertRaises(
            StrictError, loads,
            bytes('d9:キー２4:VAL19:キー１4:VAL2e', 'utf_8'), encoding='utf_8'
        )
