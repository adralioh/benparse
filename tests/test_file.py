import os
import tempfile
import unittest

import benparse


class TestBencodeFile(unittest.TestCase):
    _file_dir = os.path.join(os.path.dirname(__file__), 'files')

    _basic_path = os.path.join(_file_dir, 'basic_bencode')
    _torrent_path = os.path.join(_file_dir, 'archlinux.torrent')

    def test_load(self) -> None:
        """Load a basic file and verify its contents"""
        with open(self._basic_path, 'rb') as file:
            obj = benparse.load(file)

        self.assertEqual(obj, b'TEST_STRING')

    def test_load_exception(self) -> None:
        """Attempt to load a file opened in text mode"""
        with open(self._basic_path, 'r') as file:
            self.assertRaises(TypeError, benparse.load, file)

    def test_dump(self) -> None:
        """Dump a basic file and verify its contents"""
        with tempfile.TemporaryFile('w+b') as file:
            benparse.dump(b'TEST_STRING', file)
            file.seek(0)
            dumped_bencode = file.read()

        self.assertEqual(dumped_bencode, b'11:TEST_STRING')

    def test_dump_exception(self) -> None:
        """Attempt to dump a file opened in text mode"""
        with tempfile.TemporaryFile('w') as file:
            self.assertRaises(TypeError, benparse.dump, b'TEST_STRING', file)

    def test_torrent(self) -> None:
        """Load an actual torrent file, dump it back to bencode, and
        verify that the two are equal"""
        with open(self._torrent_path, 'rb') as file:
            original_bencode = file.read()

        obj = benparse.loads(original_bencode)
        reconverted_bencode = benparse.dumps(obj)

        self.assertEqual(original_bencode, reconverted_bencode)
