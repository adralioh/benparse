from typing import Any, Dict
import unittest

from benparse import dumps


class TestBencodeDump(unittest.TestCase):
    def test_string_byte(self) -> None:
        # basic bytes
        self.assertEqual(dumps(b'FOO'), b'3:FOO')
        # longer bytes
        self.assertEqual(
            dumps(b'THIS IS A LONG TEST STRING'),
            b'26:THIS IS A LONG TEST STRING'
        )
        # zero-length bytes
        self.assertEqual(dumps(b''), b'0:')
        # bytearray
        self.assertEqual(dumps(bytearray(b'BYTEARRAY')), b'9:BYTEARRAY')

    def test_string_unicode(self) -> None:
        # str
        self.assertEqual(dumps('UNICODESTRING'), b'13:UNICODESTRING')
        # str containing unicode characters
        self.assertEqual(
            dumps('吾輩は猫である'), bytes('21:吾輩は猫である', 'utf_8')
        )
        # explicitly provide the default encoding
        self.assertEqual(
            dumps('吾輩は猫である', encoding='utf_8'),
            bytes('21:吾輩は猫である', 'utf_8')
        )
        # different encoding
        self.assertEqual(
            dumps('吾輩は猫である', encoding='shift_jis'),
            bytes('14:吾輩は猫である', 'shift_jis')
        )
        # encoding not compatible with ascii
        self.assertEqual(
            dumps('UTF16', encoding='utf_16'),
            b'12:' + bytes('UTF16', 'utf_16')
        )

    def test_int(self) -> None:
        # basic int
        self.assertEqual(dumps(4), b'i4e')
        # large int
        self.assertEqual(dumps(928501), b'i928501e')
        # negative
        self.assertEqual(dumps(-395), b'i-395e')
        # zero
        self.assertEqual(dumps(0), b'i0e')

    def test_list(self) -> None:
        # basic list
        self.assertEqual(
            dumps([3, '', b'BYTES']), b'li3e0:5:BYTESe'
        )
        # tuple
        self.assertEqual(
            dumps((3, 'STRING', b'')), b'li3e6:STRING0:e'
        )
        # nested list
        self.assertEqual(
            dumps(
                [
                    [b'FOO', 'BAR', 34],
                    ('APPLE', b'ORANGE'),
                    'TOP'
                ]
            ),
            b'll3:FOO3:BARi34eel5:APPLE6:ORANGEe3:TOPe'
        )
        # deep nested list
        self.assertEqual(
            dumps(
                [
                    [
                        [
                            [2, 4, 6]
                        ],
                        ('A', 'B', 'C')
                    ]
                ]
            ),
            b'lllli2ei4ei6eeel1:A1:B1:Ceee'
        )
        # list of dicts
        self.assertEqual(
            dumps(
                [
                    {b'KEY1': 12, b'KEY2': 16},
                    'FOO',
                    'BAR'
                ]
            ),
            b'ld4:KEY1i12e4:KEY2i16ee3:FOO3:BARe'
        )
        # empty list
        self.assertEqual(dumps([]), b'le')
        # empty tuple
        self.assertEqual(dumps(tuple()), b'le')

    def test_dict(self) -> None:
        # basic dict
        self.assertEqual(
            dumps({
                b'KEY1': b'VALUE',
                b'KEY2': 456
            }),
            b'd4:KEY15:VALUE4:KEY2i456ee'
        )
        # str key
        self.assertEqual(
            dumps({'STRING': 'VALUE'}), b'd6:STRING5:VALUEe'
        )
        # str key with different encoding
        self.assertEqual(
            dumps({'STRING': 'VALUE'}, encoding='utf_16'),

            b'd14:' + bytes('STRING', 'utf_16') + b'12:'
            + bytes('VALUE', 'utf_16') + b'e'
        )
        # nested dicts
        self.assertEqual(
            dumps({
                b'KEY1': [b'FOO', b'BAR'],
                b'KEY2': {
                    b'KEY1': -64,
                    b'KEY2': 54
                }
            }),
            b'd4:KEY1l3:FOO3:BARe4:KEY2d4:KEY1i-64e4:KEY2i54eee'
        )
        # deep nested dicts
        self.assertEqual(
            dumps({
                'D1': {
                    'D2': {
                        'D3': {
                            'D4': {
                                'L5': [0, 6, 74],
                                b'S5': b'VALUE'
                            }
                        },
                        'S3': 'VALUE'
                    }
                }
            }),
            b'd2:D1d2:D2d2:D3d2:D4d2:L5li0ei6ei74ee2:S55:VALUEee2:S35:VALUEeee'
        )
        # sorted
        self.assertEqual(
            # ignore type error due to multiple dict key types
            dumps({  # type: ignore
                'z': 3,
                'A': 0,
                b'a': 2,
                'C': 1
            }),
            b'd1:Ai0e1:Ci1e1:ai2e1:zi3ee'
        )
        # empty dict
        # add type annotation so mypy doesn't complain
        empty_dict: Dict[bytes, Any] = {}
        self.assertEqual(dumps(empty_dict), b'de')

        # dict key is an int
        self.assertRaises(TypeError, dumps, {12: b'VALUE'})
        # dict key is a tuple
        self.assertRaises(TypeError, dumps, {(b'K1', b'K2'): b'VALUE'})
        # byte string key has the same value as a converted unicode
        # string key
        self.assertRaises(ValueError, dumps, {b'KEY': 'V1', 'KEY': 'V2'})

    def test_none(self) -> None:
        # return empty string for None
        self.assertEqual(dumps(None), b'')

        # None cannot be nested
        self.assertRaises(TypeError, dumps, [None])
        self.assertRaises(TypeError, dumps, {b'KEY': None})

    def test_invalid(self) -> None:
        # unsupported type raises exception
        self.assertRaises(TypeError, dumps, 2.5)

        # same behavior when skipinvalid is False (default)
        self.assertRaises(TypeError, dumps, 2.5, skipinvalid=False)

        # return empty string for invalid top-level type when
        # skipinvalid is True
        self.assertEqual(dumps(2.5, skipinvalid=True), b'')

        # skip invalid dict key
        self.assertRaises(
            TypeError, dumps,
            {2.5: b'VALUE', b'KEY': b'VALUE2'}, skipinvalid=False
        )
        self.assertEqual(
            dumps({2.5: b'VALUE', b'KEY': b'VALUE2'}, skipinvalid=True),
            b'd3:KEY6:VALUE2e'
        )

        # skip invalid dict value
        self.assertRaises(
            TypeError, dumps,
            {b'KEY1': 2.5, b'KEY2': b'VALUE2'}, skipinvalid=False
        )
        self.assertEqual(
            dumps({b'KEY1': 2.5, b'KEY2': b'VALUE2'}, skipinvalid=True),
            b'd4:KEY26:VALUE2e'
        )

        # skip invalid list item
        self.assertRaises(TypeError, dumps, [4, 4.5, 5, 6], skipinvalid=False)
        self.assertEqual(
            dumps([4, 4.5, 5, 6], skipinvalid=True), b'li4ei5ei6ee'
        )

        # skip invalid None
        self.assertEqual(dumps([None], skipinvalid=True), b'le')
        self.assertEqual(dumps({b'KEY': None}, skipinvalid=True), b'de')
