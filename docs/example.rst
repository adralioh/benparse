benparse example script
=======================

This example script loads an existing torrent file saved at `debian.torrent`, changes the announce URL, and dumps the new modified torrent file to `new.torrent`

.. code-block::

   import benparse

   old_torrent = 'debian.torrent'
   new_torrent = 'new.torrent'

   # Load the old torrent file
   with open(old_torrent, 'rb') as file:
       torrent_dict = benparse.load(file)

   print(torrent_dict[b'announce'])
   # Output: b'http://bttracker.debian.org:6969/announce'

   # Change the tracker announce URL
   torrent_dict[b'announce'] = b'http://mirror.example.org:6969/announce'

   # Dump the updated torrent dict to the new file
   with open(new_torrent, 'wb') as file:
       benparse.dump(torrent_dict, file)

   # Verify the changes
   with open(new_torrent, 'rb') as file:
       torrent_dict = benparse.load(file)
   print(torrent_dict[b'announce'])
   # Output: b'http://mirror.example.org:6969/announce'
