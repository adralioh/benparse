benparse documentation
======================
benparse is a bencode parser for Python 3. It is capable of reading and creating `bencoded files <https://en.wikipedia.org/wiki/Bencode>`_ such as torrents

Source and install instructions are available on `GitLab <https://gitlab.com/adralioh/benparse>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   benparse
   example script <example>
   typing
